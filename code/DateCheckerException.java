class DateCheckerException extends Exception
{
  public DateCheckerException(String message)
  {
    super(message);
  }
}
