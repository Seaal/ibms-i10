//Author: Matthew Ellwood & Alex White
//The Factory Class takes all the relevant information from the database on
//buses, drivers, routes, services and timetables and stores them in relevant
//objects


import java.util.*;

public class Factory
{ 
  //Get a driver from the database and create a Driver object for it
  public static Driver getDriverFromDatabase(int driverID)
  {
    //Get the driver info from the database
    String name = DriverInfo.getName(driverID);
    String number = DriverInfo.getNumber(driverID);
    int weeklyHours = DriverInfo.getHoursThisWeek(driverID);
    int yearlyHours = DriverInfo.getHoursThisYear(driverID);
    boolean available = DriverInfo.isAvailable(driverID);
    
    return new Driver(name, number, available, weeklyHours, yearlyHours,
                      driverID);
  }
  
  //Gets all drivers from the database and stores them in an Array of Driver
  //objects
  public static Driver[] getDrivers()
  {
    //Get the driver IDs from the database
    int[] driverIDs = DriverInfo.getDrivers();
    
    //Get the number of drivers in total
    int noDrivers = driverIDs.length;
    
    Driver[] drivers = new Driver[noDrivers];
    
    //Fill the driver array with drivers from the database
    for(int i=0;i<noDrivers;i++)
    {
      drivers[i] = getDriverFromDatabase(driverIDs[i]);
    }
    
    return drivers;
  }
  
  //Get a bus from the database and create a Bus object for it
  private static Bus getBusFromDatabase(int busID)
  {
    //Get the bus info from the database
    String number = BusInfo.busNumber(busID);
    boolean availability = BusInfo.isAvailable(busID);
    
    return new Bus(busID, number, availability);
  }
  
  //Gets all buses from the database and stores them in an Array of Bus
  //objects
  public static Bus[] getBuses()
  {
    //Get the driver IDs from the database
    int[] busIDs = BusInfo.getBuses();
    
    //Get the number of drivers in total
    int noBuses = busIDs.length;
    
    Bus[] buses = new Bus[noBuses];
    
    //Fill the driver array with drivers from the database
    for(int i=0;i<noBuses;i++)
    {
      buses[i] = getBusFromDatabase(busIDs[i]);
    }
    
    return buses;
  }
  
  //Get a bus stop from the database and store in a BusStop object
  private static BusStop getBusStopFromDatabase(int busStopID, int route)
  {
    //Get the bus stop info from the database
    String fullName = BusStopInfo.getFullName(busStopID);
    int nextStop = BusStopInfo.getNextStop(busStopID, route);
    int previousStop = BusStopInfo.getPreviousStop(busStopID, route);
    boolean timingPoint = BusStopInfo.isTimingPoint(busStopID);
    int[] routeCrossover = BusStopInfo.getRoutes(fullName);
   
    return new BusStop(fullName, nextStop, previousStop, timingPoint,
                       busStopID, routeCrossover);
  }
 
  //Gets a route from the database and stores it in a Route object
  public static Route getRoute(int routeID)
  {
    //Get the bus stop IDs from the database for this route
    int[] busStopsIDs = BusStopInfo.getBusStops(routeID);
 
    //The amount of bus stops on the route
    int noBusStops = busStopsIDs.length;
 
    //Stores all the bus stops on the route
    BusStop[] busStops = new BusStop[noBusStops];
 
    //Fill in the bus stop array with the bus stop information from the database
    for(int i = 0; i<noBusStops; i++)
    {
      busStops[i]=getBusStopFromDatabase(busStopsIDs[i],routeID);
    }
 
    //Get the route name from the database
    String routeName = BusStopInfo.getRouteName(routeID);
   
    return new Route(routeName, busStops, routeID);
  }

  public static Route[] getRoutes()
  {
    // get the route IDs
    int[] routeIDs = BusStopInfo.getRoutes();

    int noOfRoutes = routeIDs.length;

    Route[] routes = new Route[noOfRoutes];

    for(int i=0;i<noOfRoutes;i++)
    {
      routes[i] = getRoute(routeIDs[i]);
    }

    return routes;
  }
  
  //Gets a service from the database and stores it in a Service object
  private static Service getService(Route whichRoute, int id, int number,
                                    TimetableInfo.timetableKind kind)
  {
    //Get the timing stops for the service
    int[] times = TimetableInfo.getServiceTimes(whichRoute.getID(), kind, id);
    
    //Stores the bus stops which are timing points for this service
    BusStop[] timingStops = new BusStop[times.length];
    
    //Find out which bus stops corresponds to which service times
    int currentBusStop = 0;
    
    for(int i=0;i<times.length;i++)
    {
      //Skip over that bus stop if it's not a timing point
      while(!whichRoute.getBusStop(currentBusStop).isTimingPoint())
      {
        currentBusStop = (currentBusStop + 1) % whichRoute.getBusStops().length;
      }
      
      //Attach the bus stop to the time
      timingStops[i] = whichRoute.getBusStop(currentBusStop);
      
      currentBusStop = (currentBusStop + 1) % whichRoute.getBusStops().length;
    }
    
    return new Service(id, number, times, timingStops);
  }
  
  //Creates a single type of timetable for a route
  public static TimetableType getTimetableType(Route whichRoute, int type)
  {
    //Convert the type into the database enum
    TimetableInfo.timetableKind kind = TimetableType.getEnumFromType(type);
    
    //Find the total number of services on the route for that timetable type
    int noServices = TimetableInfo.getNumberOfServices(whichRoute.getID(),
                                                       kind);
    
    //Get all the services on the route for that timetable type
    int[] serviceIDs = TimetableInfo.getServices(whichRoute.getID(), kind);
    
    Service[] services = new Service[noServices];
    
    //Fill the services array with Services
    for(int i=0;i<noServices;i++)
    {
      services[i] = getService(whichRoute, i, serviceIDs[i], kind);
    }
    
    return new TimetableType(type, services);
  }
  
  //Creates a single timetable from information from the database
  private static Timetable getTimetable(Route whichRoute)
  {
    TimetableType[] types = new TimetableType[Timetable.NO_TYPES];
    
    //Create each type of timetable
    for(int i=0;i<Timetable.NO_TYPES;i++)
    {
      types[i] = getTimetableType(whichRoute, i);
    }
    
    return new Timetable(types, whichRoute);
  }
  
  //Create all the timetables from the data in the database and return as an
  //array
  public static Timetable[] getTimetables()
  {
    //Get all the routes from the database
    int[] routeIDs = BusStopInfo.getRoutes();
   
    int noRoutes = routeIDs.length;
 
    Route[] routes = new Route[noRoutes];
    
    Timetable[] timetables = new Timetable[noRoutes];
 
    //Fill the timetable array with a timetable for each route
    for(int i=0;i<noRoutes;i++)
    {
      routes[i] = getRoute(routeIDs[i]);
      timetables[i] = getTimetable(routes[i]);
    }
    
    return timetables;
  }

  public static ArrayList<BusStop> getTimeingStops(BusStop[] busStops)
  {
    ArrayList<BusStop> timeingStops = new ArrayList<BusStop>();

    for(int i=0; i<busStops.length;i++)
    {
      if(busStops[i].isTimingPoint())
        timeingStops.add(busStops[i]);
    }

    return timeingStops;
  }

   public static Hashtable<Integer,Node> getBusStopGraph()
  {
    Hashtable<Integer,Node> graph = new Hashtable<Integer,Node>();

    Route[] routes = getRoutes();
    ArrayList<BusStop> busStops;
    int[] crossovers;
    Node newNode;

    int noOfRoutes = routes.length;
    int noOfBusStopsOnRoute;
    int noOfCrossovers;

    // loop though all the current routes
    for(int i=0; i<noOfRoutes;i++)
    {
      // get all the bus stops on the route
      busStops = getTimeingStops(routes[i].getBusStops());
      noOfBusStopsOnRoute = busStops.size();
      
      for(int j=0;j<noOfBusStopsOnRoute;j++)
      {
        newNode = new Node(busStops.get(j).getID());
        graph.put(busStops.get(j).getID(),newNode);
      }
    }
      
    for(int i=0; i<noOfRoutes;i++)
    {
      // get all the bus stops on the route
      busStops = getTimeingStops(routes[i].getBusStops());
      noOfBusStopsOnRoute = busStops.size();
      
      for(int j=0;j<noOfBusStopsOnRoute;j++)
      {   
        if(j+1 < noOfBusStopsOnRoute)
        {
          graph.get(busStops.get(j).getID()).addEdge(graph.get(busStops.get(j+1).getID()));
        }

        crossovers = busStops.get(j).getCrossoverBusStopID();
        noOfCrossovers = crossovers.length;
        
        for(int k=0;k<noOfCrossovers;k++)
        {
          if(crossovers[k]!= 0)
          {
            graph.get(busStops.get(j).getID()).addEdge(graph.get(crossovers[k]));
          }
        }
      }
    }
    
    return graph;
  }
  
  
}
