import java.util.*;

public class Node implements Comparable<Node>
{
	private final int busStopID;
	private final ArrayList<Edge> edges;
	private int numEdges=0;
	private boolean visited;
	private Node previous;
	private int distance;
	
	public Node(int busStopID)
	{
		this.busStopID = busStopID;
		this.edges = new ArrayList<Edge>();
		this.visited=false;
		distance = 0;
	}

	public void addEdge(Node nodeTo)
	{
		edges.add(new Edge(nodeTo));
		numEdges++;
	}
	
	public ArrayList<Edge> getEdges()
	{
		return edges;
	}

	public Node getPrevious()
	{
	  return previous;
	}
	
	public void setPrevious(Node previous)
	{
	  this.previous = previous;
	}
	
	public int getDistance()
	{
	  return distance;
	}
	
	public void setDistance(int distance)
	{
	  this.distance = distance;
	}
	
	public int getBusStopId()
	{
		return busStopID;
	}

	public int getNumEdges()
	{
		return numEdges;
	}

	public void setVisited(boolean newVisited)
	{
		visited = newVisited;
	}

	public boolean isVisited()
	{
		return visited;
	}
	
	public int compareTo(Node other)
	{
		return this.distance - other.distance;
	}
}