//Author: Matthew Ellwood, Alex White
//Models a single day in a roster, storing a number of driverShifts on a certain
//day

import java.util.Date;

public class DayRoster
{
  private final Date day;
  private final DriverShift[] driverShifts;
  
  public DayRoster(Date day, DriverShift[] driverShifts)
  {
    this.day = day;
    this.driverShifts = driverShifts;
  }
  
  public Date getDay()
  {
    return day;
  }
  
  public DriverShift[] getDriverShifts()
  {
    return driverShifts;
  }
  
  public int getNoShifts()
  {
    return driverShifts.length;
  }
  
  public DriverShift getDriverShift(int whichShift)
  {
    return driverShifts[whichShift];
  }
}
