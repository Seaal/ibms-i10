/*
 * A very simple application illustrating how to use the interface.
 * Prints the names of all the drivers in the database.
 * @author John Sargeant
 */
import java.util.*;
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        database.openBusDatabase();
    
 /*		int[] bus = BusStopInfo.getBusStops(68);

 		int size = bus.length;
 		String  name = "'Bus Station'";

 		for(int i=0;i<size;i++)
 		{
         
         int[] crossovers = BusStopInfo.getRoutes("Stockport, Bus Station");
         int size1 = crossovers.length;

         for(int j = 0; j < size1; j++ )
         {
         	if(crossovers[j]!=0)
         	System.out.println("Bus Name" + name +" RouteNumbers: " + crossovers[j]);
     	 }

 		}
  }*/

    /* 
	   Hashtable<Integer,Node> graph = Factory.getBusStopGraph();
     RouteFinder rf = new RouteFinder();
     rf.assignTime(graph,775,1200);
     //Path p = rf.dijkstra(graph, 775, 785);

     //System.out.println(p.getRouteTaken());
      //System.out.println(p.getDistance());



      





       int index = 785;

       Node a  = (Node)graph.get(index);

       for(int i=0;i<a.getEdges().size();i++)
       {
          System.out.println(a.getBusStopId());
          System.out.println(a.getEdges().get(i).getBusStopTo());
          System.out.println(a.getEdges().get(i).getWeight());
          System.out.println();
       }

      */

      RouteFinder rf = new RouteFinder();
      
      rf.calculateTimes(770,0);
      
      Path p = rf.dijkstra(770, 797);
      
      System.out.println(p.getRouteTaken());
      System.out.println(p.getDistance());
    }

  /* public void dijkstra(ArrayList<Node> graph, int source)
    {
        int graphSize = graph.size();

        int[] distance = new int[graphSize];

        PriorityQueue queue = new PriorityQueue();

        for(int i=0;i<graphSize;i++)
        {
            distance[i] = 2*graphSize;
        }

        distance[source] =0;


    }*/

}
