//Author: Matthew Ellwood & Alex White
//Models a service as a single bus journey, with a number of timed bus stops on
//a single route

import java.util.Random;

public class Service implements Comparable
{
  private static final int MAX_DELAY = 20;
  private static final String[] cancelError = new String[]{"The driver fell over and died","Due to imminent zombie invasion", "The bus broke down", "The driver's swimming pool was on fire"};
  private static final String[] delayError = new String[]{"Rabbits everywhere", "There was an old lady on a zimmerframe crossing the road", "Driver's cat threw up", "Driver has diarrhoea"};
  private static final int numCancelErrors = 4;
  private static final int numDelayErrors = 4;
  
  private final int id;
  private final int number;
  private final int[] times;
  private final BusStop[] timingStops;
  private Bus bus;
  private boolean running;
  private int delay;
  private String errorMessage;
  
  public Service(int id, int number, int[] times, BusStop[] timingStops)
  {
    this.id = id;
    this.times = times;
    this.number = number;
    this.timingStops = timingStops;
    delay = 0;
    running = true;
    errorMessage = "";
  }
  
  public int getID()
  {
    return id;
  }
  
  public int getNumber()
  {
    return number;
  }
  
  public int getNoTimes()
  {
    return times.length;
  }
  
  public boolean isRunning()
  {
    return running;
  }
  
  public void setRunning(boolean running)
  {
    this.running = running;
  }
  
  public void randomiseDelay()
  {
    Random rand = new Random();
    delay = rand.nextInt(MAX_DELAY+1);
  }
  
  public int getDelay()
  {
    return delay;
  }
  
  public void setBus(Bus bus)
  {
    this.bus = bus;
  }
  
  public Bus getBus() throws NullPointerException
  {
    if(bus == null)
    {
      throw new NullPointerException("Bus does not exist.");
    }
    else
    {
      return bus;
    }
  }
  
  public int[] getTimes()
  {
    return times;
  } 
  
  public int getTime(int whichTime)
  {
    return times[whichTime]+delay;
  }
  
  public BusStop[] getTimingStops()
  {
    return timingStops;
  }
  
  public BusStop getTimingStop(int whichStop)
  {
    return timingStops[whichStop];
  }

  public String getErrorMessage()
  {
    return errorMessage;
  }
  
  public void setRandomCancelledReason()
  {
    Random rand = new Random();

    this.errorMessage = cancelError[rand.nextInt(numCancelErrors)];
  }

  public void setRandomDelayedReason()
  {
    Random rand = new Random();

    this.errorMessage = delayError[rand.nextInt(numDelayErrors)];
  }

  //A method to compare services so they can be sorted by their start times
  public int compareTo(Object anotherService) throws ClassCastException
  {
    if(!(anotherService instanceof Service))
    {
      throw new ClassCastException("A Service object expected.");
    }
    
    return this.times[0] - ((Service)anotherService).getTime(0);
    
  }
}
