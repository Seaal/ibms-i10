/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.Hashtable;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mammadj0
 */
public class TestFactory2 {

    public TestFactory2() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
         database.openBusDatabase();
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }


   @Test
 public void TestLiveUpdates()
 { 
  //Check whether routes array in LiveUpdates is not null 
   Route[] routes = Factory.getRoutes(); 
   assertNotNull(routes);
  //Check whether the routes taken are the actual routes 
   int[] routeIdActual;
   routeIdActual = new int[routes.length];
   for(int i = 0; i < routes.length; i++)
      routeIdActual[i] = routes[i].getID();
   int[] routeIdExpected = BusStopInfo.getRoutes();
   assertArrayEquals(routeIdExpected, routeIdActual);
   //Check whether timetableTypes can be derived 
   int dayType = LiveUpdates.getTodaysType();
   TimetableType currentType = null;
 LiveUpdates a = new LiveUpdates();
    for(int i=0;i<routes.length;i++)
        { 
           // LiveUpdates a = new LiveUpdates();
            currentType = Factory.getTimetableType(routes[i],dayType);
            assertNotNull(currentType);
            assertEquals(currentType.getType(), 0);
            //ServiceTables and tableTitles are checked
            String[][] serviceTable = a.setServiceTable(routes[i].getName());
            assertNotNull(serviceTable);
            String[] tableTitles = a.setTableTitles(routes[i].getName());
            assertNotNull(tableTitles);
        }
  //
//Testing whether impose delay working and less than 25
   Service[] services = currentType.getServices();
   assertNotNull(services);
   LiveUpdates.imposeDelays(0.1, services); 
    for(int i = 0; i < 10 ; i ++)
    {
     assertTrue(services[i].getDelay() < 22);
    }

//Testing the longest service method is ok or not
  LiveUpdates b = new LiveUpdates();
  Service longest =  b.findLongestService(services); 
 for(int i = 0; i < services.length; i ++)
 {
   assertNotNull(services[i].getTimingStops());
   assertTrue(longest.getTimingStops().length >= services[i].getTimingStops().length);
   

 }

//Check whether services can be cancelled
 for(int i = 0; i < 10; i++)
  { 
     /*
      b.cancelService(services[i].getNumber());

  assertFalse(services[i].isRunning()); */

     String status = a.getServiceStatus(services[i]);
     assertNotNull(status);
            
  }


 }





















    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

}
