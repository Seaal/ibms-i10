import java.util.*;

public class RouteFinder
{ 
  private Hashtable<Integer,Node> graph;
  private int numNodes;
  
	public RouteFinder()
	{
		 database.openBusDatabase();
		 
		 graph = Factory.getBusStopGraph();
		 numNodes = graph.size();
		 
		 ArrayList<Node> nodes = new ArrayList<Node>(graph.values());
	}

	public void calculateTimes(int busStopId, int time)
	{
	  ArrayList<Node> nodes = new ArrayList<Node>(graph.values());
	  
	  for(int i=0;i<numNodes;i++)
	  {
	    nodes.get(i).setVisited(false);
	  }
	  
	  assignTime(graph.get(busStopId), time);
	}
	
	private void assignTime(Node currentNode, int time)
	{
    if(!currentNode.isVisited())
    {
      ArrayList<Edge> currentNodeEdges = currentNode.getEdges();
      int numEdges = currentNodeEdges.size();
      
      currentNode.setVisited(true);
      
      for(int i=0;i<numEdges;i++)
      {
          int nextTime = currentNodeEdges.get(i).updateWeight(time);
          
          assignTime(currentNodeEdges.get(i).getNodeTo(),nextTime);
      }
    }
	}
	
  private void prepareNodes(PriorityQueue<Node> queue, ArrayList<Node> nodes,
                            int source)
  {
    int numNodes = nodes.size();
    
    for(int i=0;i<numNodes;i++)
  	{
  	  
  	  nodes.get(i).setPrevious(null);
  	  
  		if(nodes.get(i).getBusStopId()==source)
  		{
  			nodes.get(i).setDistance(0);
  			queue.add(nodes.get(i));
  		}	
  		else
  		{
  			nodes.get(i).setDistance(65535);
  			queue.add(nodes.get(i));
  		}	
  	}
  }
  
  private Path findPath(Node targetNode)
  {
    Path path = new Path(targetNode.getDistance());
    
    Node currentNode = targetNode;
    
    while(currentNode != null)
    {
      path.addRouteStop(currentNode.getBusStopId());
      currentNode = currentNode.getPrevious();
    }
    
    return path;
  }
  
  public Path dijkstra(int source, int target)
  {
  	PriorityQueue<Node> queue = new PriorityQueue<Node>();

  	ArrayList<Node> nodes = new ArrayList<Node>(graph.values());

  	prepareNodes(queue, nodes, source);
	
  	Node currentNode;
  	
  	do
  	{
  	  currentNode = queue.poll();

  		ArrayList<Edge> edges = currentNode.getEdges();
  		for(int i=0;i<edges.size();i++)
  		{
  			int newDistance = currentNode.getDistance() + edges.get(i).getWeight();
  			Node nextNode = edges.get(i).getNodeTo();
  			if(newDistance < nextNode.getDistance())
  			{
  			  nextNode.setDistance(newDistance);
  			  nextNode.setPrevious(currentNode);
  			}
  		}
  	}while(queue.size() > 0 && currentNode.getBusStopId() != target);
  	
  	return findPath(currentNode);
  }
}