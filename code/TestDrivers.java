//Author Matthew Ellwood
//Tests whether Drivers correctly correspond to those in the database

import java.util.Random;

public class TestDrivers
{
  public static void main(String[] args)
  {
    //Open the database
    database.openBusDatabase();
    
    //Get all the driver objects from the Factory
    Driver[] drivers = Factory.getDrivers();
    
    Random rand = new Random();
    
    System.out.println("Printing Details for 5 Random Drivers");
    System.out.println();
    
    for(int i=0;i<5;i++)
    {
      //Choose a random int between 0 and the number of drivers
      int randInt = rand.nextInt(drivers.length);
      
      System.out.println("Driver ID: " + drivers[randInt].getID());
      System.out.println("Driver Name: " + drivers[randInt].getName());
      System.out.println("Driver Number: " + drivers[randInt].getNumber());
      System.out.println();
    }
  }
}
