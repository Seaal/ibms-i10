/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * second.java
 *
 * Created on 15-Feb-2013, 10:39:50
 */

//package requestholiday;

/**
 *
 * @author mammadj0
 */

import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.text.ParseException;


public class RequestHolidayGUI extends javax.swing.JDialog {

  private int driverID;
  
  /** Creates new form second */
  public RequestHolidayGUI(java.awt.Frame parent, boolean modal, int driverID)
  {
    super(parent, modal);
    
    this.driverID = driverID;
    
    initComponents();
  }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jTextField3 = new javax.swing.JTextField();
        jTextField4 = new javax.swing.JTextField();
        startMonthsCombo = new javax.swing.JComboBox();
        startDaysCombo = new javax.swing.JComboBox();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        startYearsCombo = new javax.swing.JComboBox();
        endYearsCombo = new javax.swing.JComboBox();
        endMonthsCombo = new javax.swing.JComboBox();
        endDaysCombo = new javax.swing.JComboBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Request");

        jLabel1.setText("Name:");

        jLabel2.setText("Days remaining: ");
        
        database.openBusDatabase();
                
        jTextField3.setText(DriverInfo.getName(driverID));
        
        int daysRemaining = 25 - DriverInfo.getHolidaysTaken(driverID);
        
        jTextField4.setText("" + daysRemaining);

        jButton1.setText("Next");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        }) ;

        jTextField3.setEditable(false);

        jTextField4.setEditable(false);

        startMonthsCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        startMonthsCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startMonthsComboActionPerformed(evt);
            }
        });

        startDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        jLabel3.setText("Start date");

        jLabel4.setText("End date");

        startYearsCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2013", "2014" }));
        startYearsCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startYearsComboActionPerformed(evt);
            }
        });

        endYearsCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "2013", "2014" }));

        endMonthsCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12" }));
        endMonthsCombo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                endMonthsComboActionPerformed(evt);
            }
        });

        endDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" }));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 292, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(64, 64, 64))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 69, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jTextField3)
                            .addComponent(jTextField4, javax.swing.GroupLayout.DEFAULT_SIZE, 108, Short.MAX_VALUE))
                        .addContainerGap(286, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(startYearsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(startMonthsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(endYearsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(endMonthsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(22, 22, 22)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(endDaysCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(startDaysCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(246, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(jTextField3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTextField4, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(28, 28, 28)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(startMonthsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startDaysCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(startYearsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(37, 37, 37)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(endYearsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(endMonthsCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(endDaysCombo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(112, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(167, Short.MAX_VALUE)
                .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(157, 157, 157))
        );

        pack();
           
    }// </editor-fold>//GEN-END:initComponents
  
  //Author Jahangir, Edited by Matthew
  private Date getStartDateFromGUI() throws ParseException
  {
    //Get year, month and day input
    String year = (String) this.startYearsCombo.getSelectedItem();
    String month = (String) this.startMonthsCombo.getSelectedItem();
    String day = (String) this.startDaysCombo.getSelectedItem();
  
    //Construct date string
    String date = year + "/" + month + "/" + day;
    
    //Parse and return start date
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    
    return formatter.parse(date);
  }

  //Author Jahangir, Edited by Matthew
  private Date getEndDateFromGUI() throws ParseException
  {
    //Get year, month and day input
    String year = (String) this.endYearsCombo.getSelectedItem();
    String month = (String)this.endMonthsCombo.getSelectedItem();
    String day = (String)this.endDaysCombo.getSelectedItem();
  
    //Construct date string
    String date = year + "/" + month + "/" + day;
  
    //Parse and return start date
    SimpleDateFormat formatter = new SimpleDateFormat("yyyy/MM/dd");
    return formatter.parse(date);
  } 

  //Classifies the driver as unavailable between the from and to date and 
  //decrements the holiday days he has accordingly
  //Author Matthew
  private void writeDatesToDatabase(Date dateFrom, Date dateTo)
  throws DateCheckerException
  {
    //The amount of days taken off by the driver. Doesn't count days that were
    //already unavailable
    int daysTakenOffCount = 0;
    
    //Loop through each day setting it as unavailable to the driver
    for(Date dateLoop = dateFrom;
        dateLoop.before(HolidayChecker.addDays(dateTo,1));
        dateLoop = HolidayChecker.addDays(dateLoop,1))
    {
      if(DriverInfo.isAvailable(driverID, dateLoop))
      {
        DriverInfo.setAvailable(driverID, dateLoop, false);
        daysTakenOffCount++;
      }
        
    }
    
    if(daysTakenOffCount == 0)
    {
      throw new DateCheckerException("You are already unavailable on all of " +
                                     "these days");
    }
    
    //Update the amount of days off a driver has taken
    DriverInfo.setHolidaysTaken(driverID, daysTakenOffCount +
                                DriverInfo.getHolidaysTaken(driverID));
  }

  //Author Jahangir, Edited by Matthew
  private void jButton1ActionPerformed(java.awt.event.ActionEvent evt)  {//GEN-FIRST:event_jButton1ActionPerformed
      // TODO add your handling code here:
  
  ResponseGUI response = new ResponseGUI(new javax.swing.JFrame(), true,
                                         driverID);
      
  try
  {
    
    Date from = getStartDateFromGUI();
    Date to = getEndDateFromGUI();
    
    HolidayChecker checker = new HolidayChecker(driverID);
    
    int daysRequested = checker.check(from, to);
    
    response.jTextField1.setText("Success!");
    
    writeDatesToDatabase(from, to);
    
    response.updateDaysRemaining();
  }
  catch(ParseException e)
  {
    System.err.println( e.getMessage());
  }
  catch(DateCheckerException e)
  {
    response.jTextField1.setText(e.getMessage());
  }
  catch(InvalidQueryException e)
  {
    response.jTextField1.setText("Database Error: " + e.getMessage());
  }
  
  this.setVisible(false);
  response.setVisible(true);



}//GEN-LAST:event_jButton1ActionPerformed
        String[] date29 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29"};
        String[] date28 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28"};
        String[] date30 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30"};
        String[] date31 = {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31" };
    private void startMonthsComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startMonthsComboActionPerformed
               // TODO add your handling code here:
      
        String[] years = {"2013", "2014", "2015", "2016"};
       int a =  startMonthsCombo.getSelectedIndex();
       int b = startYearsCombo.getSelectedIndex();
      
             if(a == 0 ||a== 2 ||a== 4||a== 6 ||a== 7 ||a== 10 || a==11)
             {
                 startDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date31));
             }
             else if(a ==1)
             {
                  startDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date28));
             }
             else
                 startDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date30));


    }//GEN-LAST:event_startMonthsComboActionPerformed

    private void startYearsComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startYearsComboActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_startYearsComboActionPerformed

    private void endMonthsComboActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_endMonthsComboActionPerformed
        // TODO add your handling code here:
        int d = endMonthsCombo.getSelectedIndex();
          if(d == 0 || d== 2 || d== 4|| d== 6 || d== 7 ||d == 10 || d==11)
             {
                 endDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date31));
             }
             else if(d ==1)
             {
                  endDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date28));
             }
             else
                 endDaysCombo.setModel(new javax.swing.DefaultComboBoxModel(date30));
    }//GEN-LAST:event_endMonthsComboActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox endYearsCombo;
    private javax.swing.JComboBox endDaysCombo;
    private javax.swing.JComboBox startDaysCombo;
    private javax.swing.JComboBox endMonthsCombo;
    private javax.swing.JComboBox startMonthsCombo;
    private javax.swing.JComboBox startYearsCombo;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    public javax.swing.JTextField jTextField3;
    public javax.swing.JTextField jTextField4;
    // End of variables declaration//GEN-END:variables

}
