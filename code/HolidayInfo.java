
import java.util.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;  

 
/**
 * Class which represents information about holiday requests and handling
 * them. Most ot that is done by using the request's ID<br><br>
 * 
 * Holiday requests have an id, length, starting date and the id
 * of the driver that issued the request<br><br>
 * 
 * The methods have some checks for legality of the input and throw
 * InvalidQueryExceptions but are not enough per se<br><br><br><br>
 *
 *
 *
 * If the states for requests are needed the following classes will be implemented
 * Get state of a request
 * String getFlag(int requestID); returns declined/approved/unprocessed
 *
 * boolean isDeclined(int requestID);    returns true if declined, false otherwise
 * boolean isApproved(int requestID);    returns true if approved, false otherwise
 * boolean isUnprocessed(int requestID); returns true if unprocessed, false otherwise
 */
public class HolidayInfo 
{
  // This class is not intended to be instantiated
  private HolidayInfo() 
  { 
  }

  /**
   * Check if driver has requested a holiday
   */
  public static Boolean hasRequested(int driver)
  {
    if (driver == 0) throw new InvalidQueryException("Nonexistent driver");

    if(database.busDatabase.find_id("holiday_request", "driver", driver) != 0) return true;
    else                                                                       return false;
  }

    /**
   * Get the IDs of all requests in the database that are from that driver
   */
  public static int[] getRequests(int driver)
  {
    if (driver == 0) throw new InvalidQueryException("Nonexistent driver");

    return database.busDatabase.select_ids("holiday_request_id", "holiday_request", "driver", driver, "");

  }
  /**
   * Get the IDs of all requests in the database
   */
  public static int[] getRequests()
  {
    return database.busDatabase.select_ids("holiday_request_id", "holiday_request", "driver");
  }

  /**
   * Get driver from requestID
   */
  public static int getDriver(int request)
  {
    if (request == 0) throw new InvalidQueryException("Nonexistent request");
    return database.busDatabase.get_int("holiday_request", request, "driver");
  }

  /**
   * Get length of a holiday from requestID
   */
  public static int getLength(int request)
  {
    if (request == 0) throw new InvalidQueryException("Nonexistent request");
    return database.busDatabase.get_int("holiday_request", request, "length");
  }

  /**
   * Get starting date of a holiday from requestID
   */
  public static Date getStartingDate(int request)
  {
    if (request == 0) throw new InvalidQueryException("Nonexistent request");
    return database.busDatabase.get_date("holiday_request", request, "day");
  }  

  /**
   * Get ending date of a holiday from requestID
   */
  public static Date getEndingDate(int request)
  {
      
    if (request == 0) throw new InvalidQueryException("Nonexistent request");
    
    Date tempDate = database.busDatabase.get_date("holiday_request", request, "day");
    int length = database.busDatabase.get_int("holiday_request", request, "length");
    
   return calculateDate(tempDate, length);
  } 

  /**
   * Add days to a date
   */
  public static Date calculateDate(Date date, int length)
  {
    Calendar calendar = new GregorianCalendar();
    SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
 
    calendar.setTime(date);
    calendar.add(Calendar.DATE, length);
    return calendar.getTime();
  }
  /**
   * Add a new request.<br>
   * It will abort if the date is null, the driver is 0 
   * or if there is already a request from that driver on that date
   */
  public static int addRequest(int driver, Date startingDate, int length)
  {
    if (startingDate == null) throw new InvalidQueryException("Date is null");
    if (driver == 0) throw new InvalidQueryException("Nonexistent driver");
    if (database.busDatabase.find_id("holiday_request_id", "holiday_request", "day", startingDate, "driver", driver) > 0) throw new InvalidQueryException("A holiday request exists for that driver and date");
    else
      database.busDatabase.new_record("holiday_request", new Object[][]{{"driver", driver}, {"day", startingDate}, {"length", length}});
    return database.busDatabase.find_id("holiday_request_id", "holiday_request", "day", startingDate, "driver", driver);
  }

  /**
   * Delete a request.<br>
   * It will abort if the request is 0
   */
  public static Boolean deleteRequest(int request)
  {
    if(request == 0) throw new InvalidQueryException("Nonexistent request");

    return database.busDatabase.delete_record("holiday_request", request);
  }
}
