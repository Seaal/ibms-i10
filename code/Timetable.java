//Author: Matthew Ellwood & Alex White
//Models a timetable, all the services on different days on a single route

import java.util.Date;
import java.util.Calendar;

public class Timetable
{
  //The number of different timetable types
  public static final int NO_TYPES = 3;
  
  private final TimetableType[] types;
  private final Route route;
  
  public Timetable(TimetableType[] types, Route route)
  {
    this.types = types;
    this.route = route;
  }
  
  public TimetableType[] getTypes()
  {
    return types;
  }
  
  public Route getRoute()
  {
    return route;
  }
  
  public TimetableType getType(int whichType)
  {
    return types[whichType];
  }
  
  public TimetableType getType(Date whichDay)
  {
    //Get the time and date for now
    Calendar calendar = Calendar.getInstance();
    
    //Set the calendar to the day provided
    calendar.setTime(whichDay);
    
    //Get today's day, the int goes from 1-7, 1 being Saturday and 7 being
    //Sunday
    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    
    //Return the type dependant on what day it is
    if(dayOfWeek == 1)
    {
      return types[2];
    }
    else if(dayOfWeek == 7)
    {
      return types[1];
    }
    else
    {
      return types[0];
    }
  }
}
