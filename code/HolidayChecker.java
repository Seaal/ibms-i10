import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.util.Calendar;

//Class which checks all the requirements of the the two dates of the holiday
//Author: Matthew Ellwood

class HolidayChecker
{
  
  //The amount of drivers allowed to take a certain date off
  private static final int MAX_NO_OF_HOLIDAYS_PER_DATE = 10;
  
  //The amount of days a driver is allowed to take off consecutively
  private static final int MAX_NO_OF_CONSECUTIVE_DAYS = 7;
  
  //The amount of holidays a driver can take in a year
  private static final int MAX_NO_OF_HOLIDAYS = 25;
  
  //The database identifier of the driver
  private int driverID;
  
  //Constructor Method
  public HolidayChecker(int driverID)
  {
    this.driverID = driverID;
  }
  
  //Checks all possible errors the dates 
  public int check(Date dateFrom, Date dateTo) throws DateCheckerException
  {
    //Check that the start date is before the end date
    if(!isFromBeforeAfter(dateFrom, dateTo))
    {
      throw new DateCheckerException("The start date has to be after the end" +
                                     " date");
    }
    
    //Find the number of days requested and check they aren't more that the
    //MAX_NO_OF_CONSECUTIVE DAYS
    int noDaysRequested = calculateDaysRequested(dateFrom, dateTo);
    
    //Check if the driver has enough holidays left to take the days off
    if(!hasEnoughHolidays(noDaysRequested))
    {
      throw new DateCheckerException("You do not have enough holidays to make" +
                                     " this request");
    }
       
    //Check if the start date is in the past. We already checked if the start
    //date is before the end date, so if the start date is not in the past, then
    //the end date won't be either.
    if(isDateInPast(dateFrom))
    {
      throw new DateCheckerException("Your end date is in the past");
    }
    
    //Check whether the dates are free on those days or not
    if(!areDatesFree(dateFrom, dateTo))
    {
      throw new DateCheckerException("One of the dates is fully booked");
    }   
    
    return noDaysRequested;
  }
  
  //Adds a number of days to the date returning a new date
  public static Date addDays(Date date, int amount)
  {
    Calendar calendar = Calendar.getInstance();
    
    calendar.setTime(date);
    calendar.add(Calendar.DATE, amount);
    
    return calendar.getTime();
  }
  
  //Returns today's date
  public static Date getTodaysDate()
  {
    //Get the time and date for now
    Calendar calendar = Calendar.getInstance();
    
    //Set the time to 00:00:00 to make it just the date
    calendar.set(Calendar.HOUR_OF_DAY, 0);
    calendar.set(Calendar.MINUTE, 0);
    calendar.set(Calendar.SECOND, 0);
    
    //Return the date
    return calendar.getTime();
  }
  
  //Returns true if the date given are in the past, false otherwise
  private boolean isDateInPast(Date whichDate)
  {
    Date today = getTodaysDate();
    
    today = addDays(today,-1);
    
    return whichDate.before(today);
  }
  
  //Returns true if the driver has enough holidays left to fill the holiday
  //request, false otherwise
  private Boolean hasEnoughHolidays(int noDaysRequested)
  {
    return ((MAX_NO_OF_HOLIDAYS - DriverInfo.getHolidaysTaken(driverID))
            - noDaysRequested) > 0;
  }
  
  //Returns true if the dateTo is in the future in relation to the dateFrom
  private Boolean isFromBeforeAfter(Date dateFrom, Date dateTo)
  {
    //Check that
    return dateFrom.before(addDays(dateTo,1));
  }
  
  //Returns true if the total number of drivers that have taken the date off is
  //less than the maximum amount allowed, false otherwise
  private Boolean isDateFree(Date whichDate)
  {
    int[] driverIDs = DriverInfo.getDrivers();
    
    int noDriversUnavailable = 0;
    
    //Check if each driver has taken the date off
    for(int driver : driverIDs)
    {
      //Tally the total amount of drivers that have taken the date off
      if(!DriverInfo.isAvailable(driver, whichDate))
      {
        noDriversUnavailable++;
      }
    }
    
    //If there are more than the maximum number allowed, return false
    if(noDriversUnavailable>=MAX_NO_OF_HOLIDAYS_PER_DATE)
    {
      return false;
    }
    else
    {
      return true;
    }
  }
  
  //Returns an array if the total number of drivers that have taken the dates
  //off between the specified dates is less than the maximum amount allowed
  //false otherwise
  private Boolean areDatesFree(Date dateFrom, Date dateTo)
  {
    //Check that each individual day between the dates specified aren't fully
    //booked
    for(Date dateLoop = dateFrom; dateLoop.before(addDays(dateTo,1));
        dateLoop = addDays(dateLoop,1))
    {
       return isDateFree(dateLoop);
    }
    
    return true;
  }
  
  //For the two dates specified, calculate the amount of days that reside
  //between those dates, throws an exception if the days requested is more than
  //the total number allowed
  public int calculateDaysRequested(Date dateFrom, Date dateTo)
  throws DateCheckerException
  {
    //Create a new date by minusing To by From
    Date dateDiff = new Date(dateTo.getTime() - dateFrom.getTime());
    
    //Check how many days there are in this new date
    Calendar calendar = Calendar.getInstance();
    calendar.setTime(dateDiff);
    
    //Assume we will never allow more than 365 days off at a time
    //And check the number of days is less than the maximum allowed
    if(calendar.get(calendar.YEAR) > 1970 ||
       calendar.get(calendar.DAY_OF_YEAR) > MAX_NO_OF_CONSECUTIVE_DAYS)
    {
      throw new DateCheckerException("You cannot take more than " +
                                     MAX_NO_OF_CONSECUTIVE_DAYS +  " days off "+
                                     "Consecutively");
    }
    else
    {
      return calendar.get(calendar.DAY_OF_YEAR);
    }
  }
  
}
