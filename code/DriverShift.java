//Author: Matthew Ellwood & Alex White
//Models a driver shift, a service worked by the driver

public class DriverShift
{
	private final Driver driver;
	private final int shiftStartTime;
	private final int shiftFinishTime;
	private final int shiftLength;
	private final Service service;

	public DriverShift(int shiftFinishTime, Service service, int shiftStartTime,
	                   Driver driver)
	{
		this.shiftFinishTime = shiftFinishTime;
		this.service=service;
		this.shiftStartTime = shiftStartTime;
		this.shiftLength = shiftFinishTime - shiftStartTime;
		this.driver = driver;
	} 

	public Driver getDriver()
	{
	  return driver;
	}
	
	public Service getService()
	{
		return service;
	}

	public int getShiftFinishTime()
	{
		return shiftFinishTime;
	}

	public int getShiftStartTime()
	{
		return shiftStartTime;
	}

	public int getShiftLength()
	{
		return shiftLength;
	}
}