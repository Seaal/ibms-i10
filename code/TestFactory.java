/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
import java.util.*;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author mammadj0
 */
public class TestFactory {

    public TestFactory() {
    }

    @BeforeClass
    public static void setUpClass() throws Exception {
        database.openBusDatabase();


   
    }

    @AfterClass
    public static void tearDownClass() throws Exception {
    }

    @Test
    public void testNodeCount()
     { 
 
       int[] routes = BusStopInfo.getRoutes();
       int busStops = 0;
      for(int i=0; i<routes.length;i++)
     {
      // get all the bus stops on the route
        busStops += TimetableInfo.getTimingPoints(routes[i]).length;
     } 
     int validNumber = busStops;
     Factory test = new Factory();
     Hashtable<Integer,Node> testGraph = Factory.getBusStopGraph();
     int testLength = testGraph.size();
   //  assertEquals(validNumber, testLength);
     }
 
 
 @Test
 public void TestTimetableType()
 {
    int todaysType = 0;
    assertEquals(todaysType, LiveUpdates.getTodaysType());
}     
    
 @Test
 public void TestDijkstra()
  {
    
     RouteFinder rf = new RouteFinder();
     rf.calculateTimes(775,1200);
       
     Path path = rf.dijkstra(775, 785);
     //assertNotNull(path);
     







   }

@Test
 public void TestDijkstra2()
  {
    
     RouteFinder rf = new RouteFinder();
     rf.calculateTimes(770,0);
       
     Path p = rf.dijkstra(770, 797);
     
     ArrayList<Integer> route = p.getRouteTaken();
     
     assertNotNull(route);
     
    assertTrue(route.get(route.size() - 1) == 797);
     
     //Distance is not null in path
     assertNotNull(p.getDistance());
     
     //Routes taken is not null
     Factory test = new Factory();
     Hashtable<Integer,Node> testGraph = Factory.getBusStopGraph();
       Node a  = (Node)testGraph.get(785);
     assertNotNull(p.getRouteTaken());
     assertNotNull(rf.findPath(a));
   }

//Testing whether timing points are true in the graph
  @Test
 public void TestTimingPoint()
 {
   
    Route[] routes = Factory.getRoutes();
      ArrayList<BusStop> busStops = null;
      for(int i=0; i<routes.length;i++)
     {
      busStops = Factory.getTimeingStops(routes[i].getBusStops());
     assertNotNull(busStops);
      for(int j=0; j< busStops.size(); j++)
      {
        
       assertTrue( busStops.get(j).isTimingPoint());
      }
     } 
  }



    
















 
    @Before
    public void setUp() {

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}

}
