//Author: Matthew Ellwood, Alex White

public class Driver
{
	private final String name;
	private final String number;
	private boolean availability;
	private int nextAvailable;
	
	//The minutes and hours a driver has worked
	private int weeklyMinutes;
	private int yearlyHours;
	private int dailyMinutes;
	
	private boolean hadBreak=false;
	private final int id;

	public Driver(String name, String driverNo, boolean availability,
	              int weeklyMinutes, int yearlyHours, int id)
	{
		this.name = name;
		this.number = driverNo;
		this.availability = availability;
		this.weeklyMinutes = weeklyMinutes;
		this.yearlyHours = yearlyHours;
		this.dailyMinutes = 0;
		this.id = id;
		this.nextAvailable = -1;
	}

	public int getWeeklyMinutes()
	{
		return weeklyMinutes;
	}

	public int getDailyMinutes()
	{
		return dailyMinutes;
	}

	public int getYearlyHours()
	{
		return yearlyHours;
	}

	public boolean getAvailability()
	{
		return availability;
	}

	public String getName()
	{
		return name;
	}

	public String getNumber()
	{
		return number;
	}

	public void setAvailability(boolean availability)
	{
	  this.availability = availability;
	}
	
	public int getID()
	{
		return id;
	}

	public void resetDailyMinutes()
	{
	  dailyMinutes = 0;
	  nextAvailable = -1;
	}
	
	private void updateTimeWorked(int addedMinutes)
	{
		dailyMinutes+=addedMinutes;
		weeklyMinutes+=addedMinutes;
		yearlyHours+=addedMinutes;
	}

	public boolean isNextAvailable(int minutes)
	{
		//Returns whether a driver can work until the specified time
		return availability && nextAvailable<minutes && dailyMinutes<=600
		       && weeklyMinutes<=3000;
	}

	public void setNextAvailable(int availableTime, int duration)
	{
		
		// checks to see if the driver has taken a break
		if(duration+dailyMinutes>=300 && !hadBreak)
		{
			// adds on a breaks and shift time 
			nextAvailable = availableTime+60;
			hadBreak = true;
		}
		else
		{
			nextAvailable = availableTime;// add normal time
		}

		updateTimeWorked(duration);
	}

	private int getNextAvailable()
	{
		return nextAvailable;
	}
}