/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/*
 * TimetableGUI.java
 *
 * Created on 19-Mar-2013, 12:53:17
 */

/**
 *
 * @author mammadj0
 */
public class TimetableGUI extends javax.swing.JDialog {

    /** Creates new form TimetableGUI */
    public TimetableGUI(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        routeComboBox = new javax.swing.JComboBox();
        kindComboBox = new javax.swing.JComboBox();
        previewButton = new javax.swing.JButton();
        serviceLabel = new javax.swing.JLabel();
        dayLabel = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        table = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Timetable");

        routeComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "383", "384", "358out", "358back" }));
        routeComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                routeComboBoxActionPerformed(evt);
            }
        });

        kindComboBox.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Mon-Fri", "Sat", "Sun" }));
        kindComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                kindComboBoxActionPerformed(evt);
            }
        });

        previewButton.setText("Preview");
        previewButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                previewButtonActionPerformed(evt);
            }
        });

        serviceLabel.setText("Service:");

        dayLabel.setText("Day:");
 table.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null}
            },
            new String [] {
                "Bus Stops", "Time", "Title 3", "Title 4", "Title 5", "Title 6", "Title 7", "Title 8", "Title 9", "Title 10", "Title 11", "Title 12", "Title 13", "Title 14", "Title 15", "Title 16", "Title 17"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.String.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class, java.lang.Object.class
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }
        });
      

        jScrollPane1.setViewportView(table);
        table.getColumnModel().getColumn(0).setMinWidth(130);
        table.getColumnModel().getColumn(0).setPreferredWidth(200);
        table.getColumnModel().getColumn(2).setPreferredWidth(100);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(serviceLabel)
                        .addGap(18, 18, 18)
                        .addComponent(routeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(dayLabel)
                        .addGap(2, 2, 2)
                        .addComponent(kindComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(previewButton))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 799, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(218, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(serviceLabel)
                    .addComponent(routeComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(dayLabel)
                    .addComponent(kindComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(previewButton))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 398, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(185, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>

    private void routeComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
       
       

    }

    private void kindComboBoxActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
    }
   int[] types = {0, 1, 2};
   String[] every15= {"in" , "each" , "15" ,"min", "till" , "7pm", "", ""};
   String[] every30 = {"in" , "each" , "30" ,"min", "till" , "12pm"};
   String[] every60 = { "in" , "each" , "60" ,"min", "till" , "12pm"};
   String x = "383", y = "384", z="358out" , w ="358back";
   String l = "0", m ="1" , n = "2";
   
    private void previewButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:
        database.openBusDatabase();
        Timetable[] timetables = Factory.getTimetables();
        Object routeCombo = routeComboBox.getSelectedItem();
       // Object kindCombo = kindComboBox.getSelectedItem();
      
      
          
        createTimetable01(routeComboBox.getSelectedItem(), kindComboBox.getSelectedItem());
         // createTimetable2(routeCombo, kindCombo);
      
   
    
}//previewAction

int i=1;
int c;

public void createTimetable01(Object routName, Object kind)
 {
  for(int i=0; i<17; i++)
  {
    for(int j=0; j< 15; j++)
     table.setValueAt("", j, i );
   }
   
   i = 1;  c = 0;

 
   int id, weekDay;
   if(routName == "383")
      id = 65;
   else if(routName == "384")
       id = 66;
   else if(routName == "358out")
     id = 67;
   else
     id =68; 

  if(kind == "Mon-Fri")
     weekDay =0;
  else if(kind == "Sat")
     weekDay =1;
  else
    weekDay =2;

 Route route = Factory.getRoute(id);  
        
  TimetableType type = Factory.getTimetableType(route, weekDay);
  Service[] services = type.getServices();
           
  BusStop[] timingStop ;
 


 if((id == 65 || id ==66) && (weekDay == 0 || weekDay ==1))
 {       
 


     for(int a=0; a<services.length; a++)
     {
       timingStop = services[a].getTimingStops();

      for(int d=0; d<timingStop.length; d++)
         table.setValueAt(timingStop[d].getName(), d , 0);

        int[] times = services[a].getTimes();

        if(times[0] <420 && times[0] > 100) 
        {  
       
         for(int j=0; j<times.length; j++)
         {
            table.setValueAt(giveClockTime(times[j]), j, i );
          
         }//for
        
          i++;
        }//if
      
     }

     for(int a=0; a<8; a++)
        table.setValueAt(every15[a], a , i);
     c = i+1;
     for(int a=0; a < services.length; a++)
      {
         int[] times = services[a].getTimes();
       if(times[0] >=1140 && times[0] <= 1170)
       {
       
         for(int j=0; j<times.length; j++)
         {
            table.setValueAt(giveClockTime(times[j]), j, c );
           
         }//for 
        c++;
      }//if 
    

     }//for
     for(int a=0; a<6; a++)
         table.setValueAt(every30[a], a, c);

  }
 else
 {
   

  for(int a=0; a<services.length; a++)
     {
       timingStop = services[a].getTimingStops();

      for(int d=0; d<timingStop.length; d++){
         table.setValueAt(timingStop[d].getName(), d , 0);
         }
        int[] times = services[a].getTimes();

        if(times[0] <900 && times[0] > 100) 
        {  
       
         for(int j=0; j<times.length; j++)
         {
            table.setValueAt(giveClockTime(times[j]), j, i );
          
         }//for
        
          i++;
        }//if
      
     }
 
     for(int a=0; a<6; a++)
        table.setValueAt(every60[a], a , i);
    











 }




 }//createTimetable
 

/* public void createTimetable2(Object routName, Object kind)
 {
     for(int i=0; i<17; i++)
    {
      for(int j=0; j< 10; j++)
       table.setValueAt("", j, i );
    }
   
   i = 1;  c = 0;
  
   int id, weekDay;
   if(routName == "383")
      id = 65;
   else if(routName == "384")
       id = 66;
   else if(routName == "358out")
     id = 67;
   else
     id =68; 

  if(kind == "Mon-Fri")
     weekDay =0;
  else if(kind == "Sat")
     weekDay =1;
  else
    weekDay =2;

  Route route = Factory.getRoute(id);  
        
  TimetableType type = Factory.getTimetableType(route, weekDay);
  Service[] services = type.getServices();
           
  BusStop[] timingStop = services[0].getTimingStops();
for(int d=0; d<timingStop.length; d++)
      table.setValueAt(timingStop[d].getName(), d , 0);

   for(int a=0; a<services.length; a++)
     {
      
        int[] times = services[a].getTimes();

        if(times[0] <900 && times[0] > 100) 
        {  
       
         for(int j=0; j<times.length; j++)
         {
            table.setValueAt(giveClockTime(times[j]), j, i );
          
         }//for
        
          i++;
        }//if
      
     }

     for(int a=0; a<6; a++)
        table.setValueAt(every60[a], a , i);
    

















 } */




















  public String giveClockTime(int time)
  {
    int hours= time /60;
    int minutes=time %60;
   String clockTime= hours + ":" + minutes; 
    if(minutes < 10)
        clockTime= hours + ":0" + minutes; 
    return clockTime;
  }



    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
       
               
   
        java.awt.EventQueue.invokeLater(new Runnable() {
 
            public void run() {
                TimetableGUI dialog = new TimetableGUI(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify
    private javax.swing.JLabel dayLabel;
    private javax.swing.JComboBox routeComboBox;
    private javax.swing.JComboBox kindComboBox;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton previewButton;
    private javax.swing.JLabel serviceLabel;
    private javax.swing.JTable table;
    // End of variables declaration

}

