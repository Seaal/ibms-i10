//Author Matthew Ellwood, Alex White, Kaarel Holm
//Models a route, a collection of bus stops, which a bus drives upon

public class Route
{
  private final int id;
  private final String name;
  private final BusStop[] busStops;

  public Route(String name, BusStop[] busStops, int id)
  {
    this.id = id;
    this.name = name;
    this.busStops = busStops;
  }
  
  public BusStop[] getBusStops()
  {
    return busStops;
  }
  
  public int getNoBusStops()
  {
    return busStops.length;
  }
  
  public BusStop getBusStop(int whichBusStop)
  {
    return busStops[whichBusStop];
  }
  
  public String getName()
  {
    return name;
  }
  
  public int getID()
  {
    return id;
  }
}
  
