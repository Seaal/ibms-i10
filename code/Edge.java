public class Edge
{
	private int weight;
	private final Node nodeTo;

	public Edge(Node nodeTo)
	{
		this.weight = 0;
		this.nodeTo =nodeTo;
	}

	public int getWeight()
	{
		return weight;
	}

	public Node getNodeTo()
	{
		return nodeTo;
	}

	public int updateWeight(int startTime)
	{
		int nextTime = BusStopInfo.getNextTime(nodeTo.getBusStopId(),startTime)[0];	
		this.weight = nextTime- startTime;
		
		if(weight < 0)
		{
		  weight += 1440;
		}
		
		System.out.println(""+weight);
		
		return nextTime;
	}
}