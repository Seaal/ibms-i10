public class TestDriverHours
{
  public static void main(String[] args)
  {
    //Open the database
    database.openBusDatabase();
    
    System.out.println("Testing daily hours worked");
    System.out.println("Drivers below are those who have worked over 10 hours");
    System.out.println();
    
    //Get a days roster
    RosterHandler rh = new RosterHandler();
    
    rh.generateRosterForDay(HolidayChecker.getTodaysDate(),new IntHolder(0));
    
    Driver[] drivers = rh.getDrivers();
    
    //Print drivers who have worked more than limit for day
    for(int i=0;i<drivers.length;i++)
    {    
      if(drivers[i].getDailyMinutes() > 600)
      {
        System.out.println("Driver Name: " + drivers[i].getName());
        System.out.println("Minutes worked today: " +
                           drivers[i].getDailyMinutes());
        System.out.println();
      }
    }
    
    //Get a weeks roster
    RosterHandler rh2 = new RosterHandler();
    
    rh2.generateRosterForWeek(HolidayChecker.getTodaysDate());
    
    drivers = rh.getDrivers();
    
    System.out.println("Testing weekly hours worked");
    System.out.println("Drivers below are those who have worked over 10 hours");
    System.out.println();
    
    //Print drivers who have worked more than limit for week
    for(int i=0;i<drivers.length;i++)
    {    
      if(drivers[i].getWeeklyMinutes() > 3000)
      {
        System.out.println("Driver Name: " + drivers[i].getName());
        System.out.println("Minutes worked this week: " +
                           drivers[i].getWeeklyMinutes());
        System.out.println();
      }
    }
  }
}
