public class TestBusStops
{
  public static void main(String[] args)
  {
    //Open the database
    database.openBusDatabase();
    
    Timetable[] timetables = Factory.getTimetables();
    
    Route[] routes = new Route[2];
    
    //Extract routes from timetables
    routes[0] = timetables[0].getRoute();
    routes[1] = timetables[2].getRoute();
    
    System.out.println("Testing Routes and Bus Stops");
    System.out.println();
    
    for(int i=0;i<2;i++)
    {
      System.out.println("Route ID: " + routes[i].getID());
      System.out.println("Route Name: " + routes[i].getName());
      System.out.println();
      
      for(int j=0;j<3;j++)
      {
        BusStop bs = routes[i].getBusStop(j);
        System.out.println("  BusStop ID: " + bs.getID());
        System.out.println("  BusStop Name: " + bs.getName());
        System.out.println("  BusStop Timing Point: " + bs.isTimingPoint());
        System.out.println();
      }
      
      System.out.println();
      System.out.println();
    }
  }
}
