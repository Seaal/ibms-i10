/**
 *
 * @author mammadj0
 */
public class front extends javax.swing.JDialog {

    /** Creates new form front */
    public front(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">
    private void initComponents() {

        controllerButton = new javax.swing.JButton();
        driverButton = new javax.swing.JButton();
        PassengerButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("");
        setBackground(javax.swing.UIManager.getDefaults().getColor("Button.shadow"));

        controllerButton.setBackground(new java.awt.Color(140, 1, 127));
        controllerButton.setFont(new java.awt.Font("DejaVu Sans Light", 1, 24)); // NOI18N
        controllerButton.setForeground(new java.awt.Color(254, 226, 226));
        controllerButton.setText("Controller");
        controllerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                controllerButtonActionPerformed(evt);
            }
        });

        driverButton.setBackground(new java.awt.Color(140, 1, 127));
        driverButton.setFont(new java.awt.Font("DejaVu Sans Light", 1, 24)); // NOI18N
        driverButton.setForeground(new java.awt.Color(254, 226, 226));
        driverButton.setText("Driver");

        PassengerButton.setBackground(new java.awt.Color(140, 1, 127));
        PassengerButton.setFont(new java.awt.Font("DejaVu Sans Light", 1, 24)); // NOI18N
        PassengerButton.setForeground(new java.awt.Color(254, 226, 226));
        PassengerButton.setText("Passenger");
        PassengerButton.setBorder(new javax.swing.border.MatteBorder(null));
        PassengerButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                PassengerButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(134, 134, 134)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(driverButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                    .addComponent(PassengerButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE)
                    .addComponent(controllerButton, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 206, Short.MAX_VALUE))
                .addGap(174, 174, 174))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(33, 33, 33)
                .addComponent(controllerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(35, 35, 35)
                .addComponent(driverButton, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33)
                .addComponent(PassengerButton, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(77, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>

    private void PassengerButtonActionPerformed(java.awt.event.ActionEvent evt) {
     // TODO add your handling code here:
       

    }

    private void controllerButtonActionPerformed(java.awt.event.ActionEvent evt) {
        // TODO add your handling code here:

         front exsistingFront = new front(new javax.swing.JFrame(), true);
        exsistingFront.setVisible(false);
        ControllerGUI controllerDialog = new ControllerGUI(new javax.swing.JFrame(), true);
        controllerDialog.setVisible(true);
       
    }

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                front dialog = new front(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify
    private javax.swing.JButton controllerButton;
    private javax.swing.JButton driverButton;
    private javax.swing.JButton PassengerButton;
    // End of variables declaration

}

