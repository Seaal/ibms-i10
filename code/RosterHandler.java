//Author: Matthew Ellwood, Alex White
//The RosterHandler handles the creating of rosters

import java.util.Date;
import java.util.Calendar;
import java.util.Arrays;

public class RosterHandler
{
  //The Drivers
  Driver[] drivers;

  //Constructor
  public RosterHandler()
  {
    //Open the database
    database.openBusDatabase();
    
    drivers = Factory.getDrivers();
  }
  
  public DayRoster[] generateRosterForWeek(Date day)
  { 
    //Get the time and date for now
    Calendar calendar = Calendar.getInstance();

    //Set the calendar to the day provided
    calendar.setTime(day);
    
    
    int dayOfWeek = calendar.get(Calendar.DAY_OF_WEEK);
    
    int difference = 1 - dayOfWeek;
    
    //Find the beginning of this week
    Date weekBeginning = HolidayChecker.addDays(day,difference);
    
    IntHolder startingDriver = new IntHolder(0);
    
    DayRoster[] dayRosters = new DayRoster[8];
    
    //Generate the roster for every day this week
    for(int i=1;i<8;i++)
    {
       dayRosters[i] = generateRosterForDay(
                                     HolidayChecker.addDays(weekBeginning, i-1),
                                       startingDriver);
    }
    
    return dayRosters;
  }
  
  private void updateDriverForDay(Date day)
  {
    for(int i=0;i<drivers.length;i++)
    {
      drivers[i].setAvailability(DriverInfo.isAvailable(drivers[i].getID(),
                                                        day));
      drivers[i].resetDailyMinutes();
    }
  }
  
  public DayRoster generateRosterForDay(Date day, IntHolder startingDriver)
  {
    //Update the drivers availabilities and daily minutes worked according to 
    //today
    updateDriverForDay(day);
    
    //Array of all Timetables
    Timetable[] timetables = Factory.getTimetables();
  
    //Array of all Buses
    Bus[] buses = Factory.getBuses();

    int currentBus = 0;
    
    int noServices = 0;
    
    //Find the total number of services for that day
    for(int i=0;i<timetables.length;i++)
    {
      noServices+= TimetableInfo.getNumberOfServices(
                                         timetables[i].getRoute().getID(), day);
    }

    //Array to hold all the driver shifts
    DriverShift[] driverShift = new DriverShift[noServices];
    
    Service[] allServices = new Service[noServices];
    
    int currentNoServices = 0;
    
    //Get all the services for the day
    for(int i=0;i<timetables.length;i++)
    {
      TimetableType todaysType = timetables[i].getType(day);
      
      for(int j=0;j<todaysType.getServices().length;j++)
      {
        allServices[currentNoServices] = todaysType.getService(j);
        currentNoServices++;
      }
    }
       
    //Sort the services into the time they start
    Arrays.sort(allServices);
    
    int currentDriver = startingDriver.get();
    int currentShiftTime=0;
    
    //For each service, assign a bus and a driver
    for(int i=0;i<noServices;i++)
    {
      //When the service ends
      int shiftEndTime = allServices[i].getTime(allServices[i].getNoTimes()-1);
      
      //When the shift starts
      int shiftStartTime = allServices[i].getTime(0);
                          
      //Find the first available bus
      while(!buses[currentBus].isAvailable(allServices[i].getTime(0)))
      {
        currentBus = (currentBus + 1) % buses.length;
      }
      
      //Assign the bus to the service
      allServices[i].setBus(buses[currentBus]);
      
      //Update when the bus is next available
      buses[currentBus].setNextAvailable(shiftEndTime); 
      
      while(!drivers[currentDriver].isNextAvailable(shiftStartTime))
      {
        currentDriver = (currentDriver + 1) % drivers.length;
      }
      
      int shiftLength = shiftEndTime - shiftStartTime;
      
      drivers[currentDriver].setNextAvailable(shiftEndTime, shiftLength);

      driverShift[i] = new DriverShift(shiftEndTime, allServices[i], 
                                       shiftStartTime, drivers[currentDriver]);

      currentBus = (currentBus + 1) % buses.length; 
      currentDriver = (currentDriver + 1) % drivers.length;
    }
    
    //Remember the last driver for next time
    startingDriver.set(currentDriver);
    
    return new DayRoster(day, driverShift);
  }
  
  public Driver[] getDrivers()
  {
    return drivers;
  }
}
