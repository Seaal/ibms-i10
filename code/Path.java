import java.util.*;

public class Path
{
	private final int distance;
	private ArrayList<Integer> routeTaken;

	public Path(int distance)
	{
	  this.distance = distance;
	  routeTaken = new ArrayList<Integer>();
	}

	public int getDistance()
	{
		return distance;
	}

	public void addRouteStop(int busStopID)
	{
		routeTaken.add(busStopID);
	}

	public ArrayList<Integer> getRouteTaken()
	{
		return routeTaken;
	}
}