//Author: Matthew Ellwood & Alex White
//Models a timetable type, all the services that run on a particular day

import java.util.Date;

public class TimetableType
{
  //Converts from an int to the timetableKind enum for interfacing with the 
  //database wrapper classes
  public static TimetableInfo.timetableKind getEnumFromType(int type)
  {
    TimetableInfo.timetableKind kind;
    
    switch(type)
    {
      case 0:  kind = TimetableInfo.timetableKind.weekday;
               break;
      case 1:  kind = TimetableInfo.timetableKind.saturday;
               break;
      case 2:  kind = TimetableInfo.timetableKind.sunday;
               break;
      default: kind = TimetableInfo.timetableKind.weekday;
               break;
    }
    
    return kind;
  }
  
  // The type of timetable stored, depending on the day of the week.
  // 0 - weekday
  // 1 - saturday
  // 2 - sunday
  private final int type;
  private final Service[] services;
  
  public TimetableType(int type, Service[] services)
  throws IllegalArgumentException
  {
    if(type > 2 || type < 0)
    {
      throw new IllegalArgumentException("Type has to be between 0 and 2");
    }
    
    this.type = type;
    this.services = services;
  }
  
  public int getType()
  {
    return type;
  }
  
  public Service[] getServices()
  {
    return services;
  }
  
  public Service getService(int whichService)
  {
    return services[whichService];
  }
  
  public int getNoServices()
  {
    return services.length;
  }
}
