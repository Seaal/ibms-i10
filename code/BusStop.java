//Author Matthew Ellwood, Alex White

public class BusStop
{
  private final int id;
	private final String name;
	private final int nextStop;
	private final int previousStop;
	private final boolean timingPoint;
	private final int[] crossoverBusStopID;

	public BusStop(String name, int nextStop, int previousStop,
	               boolean timingPoint, int id, int[] crossoverBusStopID)
	{
	  this.id = id;
		this.name = name;
		this.nextStop = nextStop;
		this.previousStop = previousStop;
		this.timingPoint = timingPoint;
		this.crossoverBusStopID = crossoverBusStopID;
	}

	public int getID()
	{
		return id;
	}
	
	public String getName()
	{
		return name;
	}

	public int getNextStop()
	{
		return nextStop;
	}
	

	public int previousStop()
	{
		return previousStop;
	}

	public boolean hasNextStop()
	{
		return nextStop!=0;
	}

	public boolean isTimingPoint()
	{
		return timingPoint;
	}

	public int[] getCrossoverBusStopID()
	{
		return crossoverBusStopID;
	}
}