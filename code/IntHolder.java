//Author Matthew Ellwood
//Stores a single int so it can be passed by reference

public class IntHolder
{
  int integer;
  public IntHolder(int i)
  {
    integer = i;
  }
  
  public int get()
  {
    return integer;
  }
  
  public void set(int i)
  {
    integer = i;
  }
}
