//Author: Matthew Ellwood, Alex White

public class Bus
{
  private final int id;
  private final String number;
  private final boolean availability;
  
  //When the bus is next available at minutes past midnight. If -1 the driver
  //is available
  private int nextAvailable;
  
  public Bus(int id, String number, boolean availability)
  {
    this.id = id;
    this.number = number;
    this.availability = availability;
    nextAvailable = -1;
  }
  
  public int getID()
  {
    return id;
  }
  
  public String getNumber()
  {
    return number;
  }
  
  public boolean getAvailability()
  {
    return availability;
  }
  
  public boolean isAvailable(int minutes)
  {
    return availability && nextAvailable<minutes;
  }
  
  //Set the buses next available time. If it is set to -1 it's available
  public void setNextAvailable(int minutes)
  {
    nextAvailable = minutes;
  }
  
  public int getNextAvailable()
  {
    return nextAvailable;
  }
}
