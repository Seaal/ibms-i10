//Author Matthew Ellwood
//Tests whether Buses correctly correspond to those in the database

import java.util.Random;

public class TestBuses
{
  public static void main(String[] args)
  {
    //Open the database
    database.openBusDatabase();
    
    //Get all the driver objects from the Factory
    Bus[] buses = Factory.getBuses();
    
    Random rand = new Random();
    
    System.out.println("Printing Details for 5 Random Buses");
    System.out.println();
    
    for(int i=0;i<5;i++)
    {
      //Choose a random int between 0 and the number of drivers
      int randInt = rand.nextInt(buses.length);
      
      System.out.println("Bus ID: " + buses[randInt].getID());
      System.out.println("Bus Number: " + buses[randInt].getNumber());
      System.out.println();
    }
  }
}
